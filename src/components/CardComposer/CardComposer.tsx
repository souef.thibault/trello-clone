import { useState } from 'react';
import styles from './CardComposer.module.css';

interface CardComposerProps {
  addItem: (columnName: string, description: string) => void;
  columnName: string;
}

const CardComposer = ({ addItem, columnName }: CardComposerProps) => {
  const [showForm, setShowForm] = useState(false);
  const [descriptionText, setDescriptionText] = useState('');

  const closeCard = () => {
    setShowForm(false);
    setDescriptionText('');
  };

  const addCard = () => {
    addItem(columnName, descriptionText);
    closeCard();
  };

  return showForm ? (
    <div>
      <textarea
        className={styles.textarea}
        value={descriptionText}
        onChange={(e) => setDescriptionText(e.target.value)}
        placeholder="Enter your text here..."
      ></textarea>
      <button className={styles.button} onClick={addCard}>
        Create
      </button>
      <button className={`${styles.button} ${styles.red}`} onClick={closeCard}>
        Cancel
      </button>
    </div>
  ) : (
    <span onClick={() => setShowForm(true)} className={styles.addCard}>
      Add a card...
    </span>
  );
};

export default CardComposer;
