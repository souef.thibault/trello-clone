import styles from './Header.module.css';

const Header = () => <h1 className={styles.header}>Trello clone</h1>;

export default Header;
