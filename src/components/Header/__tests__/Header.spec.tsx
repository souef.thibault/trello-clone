import React from 'react';
import { render, screen } from '@testing-library/react';
import Header from '../Header';

test('renders Header correctly', () => {
  render(<Header />);
  const linkElement = screen.getByText(/Trello clone/i);
  expect(linkElement).toBeInTheDocument();
});
