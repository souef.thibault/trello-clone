import Card from '../Card';
import CardComposer from '../CardComposer';
import { Item } from '../../typings/item';
import styles from './Column.module.css';

interface ColumnProps {
  title: string;
  items: Item[];
  onDragOver: (e: React.DragEvent<HTMLElement>) => void;
  onDrop: (e: React.DragEvent<HTMLElement>, column: string) => void;
  onDragStart: (id: number, column: string) => void;
  addItem: (columnName: string, description: string) => void;
}

const Column = ({ title, items, onDragOver, onDragStart, onDrop, addItem }: ColumnProps) => {
  return (
    <li className={styles.column} key={title} onDragOver={onDragOver} onDrop={(e) => onDrop(e, title)}>
      <h2 className={styles.title}>{title}</h2>
      {items
        .filter((item) => item.column === title)
        .map((item) => (
          <Card onDragStart={onDragStart} description={item.value} key={item.id} id={item.id} column={item.column} />
        ))}
      <CardComposer columnName={title} addItem={addItem} />
    </li>
  );
};

export default Column;
