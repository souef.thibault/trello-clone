import { useState } from 'react';
import initialColumns from '../../datas/columns.json';
import initialItems from '../../datas/items.json';
import Column from '../Column';
import ColumnComposer from '../ColumnComposer';
import Header from '../Header';
import styles from './Board.module.css';
import useLocalStorage from '../../hooks/useLocalStorage';

interface DragData {
  id?: number;
  initialColumn?: string;
}

const Board = () => {
  const [items, setItems] = useLocalStorage('items', initialItems);
  const [columns, setColumns] = useLocalStorage('columns', initialColumns);
  const [dragData, setDragData] = useState<DragData>({});

  const handleDragStart = (id: number, column: string) => {
    setDragData({ id: id, initialColumn: column });
  };

  const handleDragOver = (e: React.DragEvent<HTMLElement>) => {
    e.preventDefault();
  };

  const changeColumn = (itemId: number, column: string) => {
    const newItems = [...items];
    newItems[itemId - 1].column = column;
    setItems([...newItems]);
  };

  const handleDrop = (e: React.DragEvent<HTMLElement>, column: string) => {
    if (dragData?.id) {
      const selected = dragData.id;
      changeColumn(selected, column);
    }
  };

  const addItem = (columnName: string, description: string) => {
    const newItem = {
      id: items.length + 1,
      column: columnName,
      value: description
    };
    const newItems = [...items, newItem];
    setItems(newItems);
  };

  const addColumn = (newColumn: string) => {
    const newColumns = [...columns, newColumn];
    setColumns(newColumns);
  };

  return (
    <section className={styles.boardContainer}>
      <header className={styles.headerContainer}>
        <Header />
      </header>
      <main>
        <ul className={styles.columnsContainer}>
          {columns.map((col) => (
            <Column
              key={col}
              items={items}
              title={col}
              onDragOver={handleDragOver}
              onDrop={handleDrop}
              onDragStart={handleDragStart}
              addItem={addItem}
            />
          ))}
          <li className={styles.columnComposerContainer}>
            <ColumnComposer addColumn={addColumn} />
          </li>
        </ul>
      </main>
    </section>
  );
};

export default Board;
