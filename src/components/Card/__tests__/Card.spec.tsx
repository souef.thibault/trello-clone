import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import Card from '../Card';

describe('Card component', () => {
  test('renders Card correctly', () => {
    render(<Card description="this is my description" id={1} column={'Column'} onDragStart={() => {}} />);
    const linkElement = screen.getByText(/this is my description/i);
    expect(linkElement).toBeInTheDocument();
  });

  test('Edit Card item on Click', () => {
    render(<Card description="this is my description" id={1} column={'Column'} onDragStart={() => {}} />);
    fireEvent.click(screen.getByText(/this is my description/i));
    expect(screen.getByLabelText('edit-item')).toBeInTheDocument();
  });
});
