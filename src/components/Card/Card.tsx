import React, { useState, KeyboardEvent } from 'react';
import styles from './Card.module.css';

interface CardProps {
  description: string;
  id: number;
  column: string;
  onDragStart: (id: number, column: string) => void;
}

const Card = ({ description, id, column, onDragStart }: CardProps) => {
  const [onEdit, setOnEdit] = useState(false);
  const [value, setValue] = useState('');

  const handleKeyDown = (event: KeyboardEvent) => {
    if (event.keyCode === 13) {
      // editItem(column, value, id); // No time to finish
      setOnEdit(false);
    }
  };

  return !onEdit ? (
    <div
      onClick={() => setOnEdit(true)}
      draggable
      onDragStart={() => onDragStart(id, column)}
      className={styles.cardContainer}
    >
      <p>{description}</p>
    </div>
  ) : (
    <input
      type="text"
      value={value}
      aria-label="edit-item"
      onChange={(e) => setValue(e.target.value)}
      onKeyDown={(e) => handleKeyDown(e)}
    />
  );
};

export default Card;
