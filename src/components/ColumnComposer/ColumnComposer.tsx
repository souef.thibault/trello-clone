import { useState } from 'react';
import styles from './ColumnComposer.module.css';

interface ColumnComposerProps {
  addColumn: (newColumn: string) => void;
}

const ColumnComposer = ({ addColumn }: ColumnComposerProps) => {
  const [showForm, setShowForm] = useState(false);
  const [columnName, setColumnName] = useState('');

  const closeCard = () => {
    setShowForm(false);
    setColumnName('');
  };

  const addNewColumn = () => {
    addColumn(columnName);
    closeCard();
  };

  return showForm ? (
    <div>
      <textarea
        className={styles.textarea}
        value={columnName}
        onChange={(e) => setColumnName(e.target.value)}
        placeholder="Enter your column here..."
      ></textarea>
      <button className={styles.button} onClick={addNewColumn}>
        Create
      </button>
      <button className={`${styles.button} ${styles.red}`} onClick={closeCard}>
        Cancel
      </button>
    </div>
  ) : (
    <span onClick={() => setShowForm(true)} className={styles.addCard}>
      Add a Column...
    </span>
  );
};

export default ColumnComposer;
