export interface Item {
  id: number;
  column: string;
  value: string;
}
