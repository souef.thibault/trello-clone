# Trello-Clone

Technical test for travaux.com

## Demo

You can try the webapp on this website:

https://infallible-bell-f2c228.netlify.app/

## Installation

```bash
  npm install
  npm run start
```

Go on http://localhost:3000/

## How to use

- You can drag and drop all items when cursor is pointer, inside columns.
- You can add a Card, edit when you click inside (WIP)
- You can add columns
- LocalStorage is available so if you want to reset your datas: `CMD+J -> Application -> LocalStorage -> Clean`

## My philosophy

I'd like to explain my choices, I preferred not to use react context in this application for state management. For me, the context is not adapted to this kind of case. If I see in the long term, the application can evolve, and the use of context can be problematic.
I use it only in a condition where the data does not change or very little (theme, or user profile). If I had to improve the application with state management, I would surely go with a RTK or another state management. I already work with context and when you use it for state management you have many cons.
Like re-rendering, performance, debugging... Maybe he is not objective (Redux Guru) but I found this article very interesting https://blog.isquaredsoftware.com/2021/01/context-redux-differences/

I used hooks already available on https://usehooks-ts.com/ to be quicker for localStorage.

I put an horizontal scrollbar when you have many columns to manage small device first. (Hard to do good UX for mobile with Trello, I copy the original behaviour version)

## Roadmap

- Edition and deletion for Card and column
- Add a storybook
- Improve performance with reactDevTools and see re-rendering, add useMemo or useCallback when it's necessary
- Add a custom theme with data-theme on CSS with Context (by example)
- Add Cypress or another tools for end to end testing
- Add a gitlab-ci for launch test and linter
- More unit testing with RTL
- Maybe use Tailwind for CSS to optimize my bundle.
- Implement renovate to update automatically my libs with PR.
- Maybe use some libraries for state management or DND. Sometimes It's better to use something already approved than re create from scratch.
- And if project become bigger, had monitoring like sitespeed and grafana for metrics.
- If datas come from API, maybe I'll use React-Query (big Fan) for state management.

## Finally

Thanks for your technical test! I enjoyed it! :)

Thibault
